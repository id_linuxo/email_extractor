<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Gets email from .eml files
 *
 * This simple script searches for "Reply-to: " string into .eml files into a directory and save them into a csv file
 *
 *
 * PHP version 7
 *
 *
 * @category   Script  
 * @author     Natale Mazza <linuzzin@gmail.com>
 * @copyright   
 * @version    1.0 
 */

/*
 * Settings
 */
define('DIR_TO_SCAN', '.');
define('FILE_OF_REPLIES', './reply_to.csv');
define('FIELD_TO_SEARCH', 'Reply-To: ');


$handle = fopen(FILE_OF_REPLIES, 'w');

$arr_eml_files = scandir(DIR_TO_SCAN);
foreach($arr_eml_files as $kf => $vf){
    if(stripos($vf, '.eml')){
        $arr_contents = file("./$vf");
        $arr_froms = array_filter($arr_contents, 'getFrom');
        
        foreach($arr_froms as $k => $v){
            $email = str_replace(FIELD_TO_SEARCH, '', $v);
            fwrite($handle, $email);
        }
         
    }
}
fclose($handle);

function getFrom($string)
{
    return (stripos($string, FIELD_TO_SEARCH) !== false);
}
?>
